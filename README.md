### First need to set up all clients (like postgres, kafka, etc...)
```shell script
git clone https://gitlab.com/it.devel.att/go_base.git
cd go_base/
make docker-compose-up-clients
```
After set up clients open new terminal
```shell script
make docker-compose-post-service-up
```
> This command will build base image with go modules (it can take a long time), image with postgres migrate tool and finally post-service 

After all things done you can check it with curl for example (In new terminal windon)
> For create post
```shell script
curl -X POST -H "Content-Type: application/json" -d '{"title": "Some title", "description": "Some description"}' localhost:9191/api/v1/posts
```
> For get posts
```shell script
curl -GET localhost:9191/api/v1/posts
```