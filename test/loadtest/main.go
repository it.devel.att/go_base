package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

func main() {
	goroutines := 10
	url := "http://localhost:9999/api/v1/posts"
	interruptCH := make(chan os.Signal)
	wg := &sync.WaitGroup{}
	wg.Add(goroutines)
	ctx, cancel := context.WithCancel(context.Background())

	transport := http.DefaultTransport.(*http.Transport).Clone()
	transport.MaxConnsPerHost = 10
	transport.MaxIdleConns = 10
	transport.MaxIdleConnsPerHost = 10
	client := &http.Client{
		Transport: transport,
		Timeout:   time.Second * 5,
	}
	for i := 0; i < goroutines; i++ {
		go func(i int) {
			defer wg.Done()

			for {
				select {
				case <-ctx.Done():
					return
				default:
					time.Sleep(time.Second)
					start := time.Now()
					response, err := client.Get(url)
					end := time.Since(start)
					if err != nil{
						fmt.Printf("[%v] Request duration: %v. Error %v\n", i, end, err)
						continue
					}
					if response.StatusCode >= 500 {
						fmt.Printf("[%v] Request duration: %v. 500 >= status code\n", i, end)
						continue
					}
					fmt.Printf("[%v] Request duration: %v\n", i, end)
				}
			}
		}(i)
	}
	signal.Notify(interruptCH, syscall.SIGINT, syscall.SIGTERM)
	<-interruptCH
	fmt.Println("receive interrupt signal")
	cancel()
	wg.Wait()
}
