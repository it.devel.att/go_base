# FOR example app
MIGRATION_FORMAT=sql
DIR_SOURCE=${MIGRATION_DIR_SOURCE}
DATABASE_DSN=${MIGRATION_DATABASE_DSN}

POST_SERVICE_DOCKERFILE=build/package/post-service/Dockerfile
POST_SERVICE_IMAGE=post-service:latest

BASE_MODULES_DOCKERFILE=build/package/base-modules/Dockerfile
BASE_MODULES_IMAGE=base-modules:latest

MIGRATE_POSTGRES_DOCKERFILE=build/package/migrate-postgres/Dockerfile
MIGRATE_POSTGRES_IMAGE=migrate-postgres:latest

env:
	cp .env.example .env

# Migration commands
create_migration:
	migrate create -ext ${MIGRATION_FORMAT} -dir ${DIR_SOURCE} -seq $(name)

migrate_up:
	migrate -database "${DATABASE_DSN}" -path ${DIR_SOURCE} up $(count)

migrate_down:
	migrate -database "${DATABASE_DSN}" -path ${DIR_SOURCE} down $(count)

build-base-modules:
	docker build -f ${BASE_MODULES_DOCKERFILE} -t ${BASE_MODULES_IMAGE} .

build-migrate-postgres:
	docker build -f ${MIGRATE_POSTGRES_DOCKERFILE} -t ${MIGRATE_POSTGRES_IMAGE} .

build-post-app:
	docker build \
		-f ${POST_SERVICE_DOCKERFILE} \
		-t ${POST_SERVICE_IMAGE} \
		--build-arg POSTGRES_MIGRATE=${MIGRATE_POSTGRES_IMAGE} \
		--build-arg BASE_MODULES_IMAGE=${BASE_MODULES_IMAGE} \
		.

docker-compose-up-clients:
	docker-compose -f deployments/docker-compose.yml up

docker-compose-post-service-up:
	docker-compose -f deployments/docker-compose.post-service.yml up
