package message_queue

import (
	"fmt"
	"strings"
	"time"

	"github.com/Shopify/sarama"

	"go_base/pkg/configs"
)

type KafkaClient struct {
	Client sarama.Client
}

func NewKafkaClient(cfg configs.Kafka) (*KafkaClient, error) {
	c := sarama.NewConfig()
	c.Producer.Retry.Max = 10
	c.Producer.Return.Successes = true
	c.Producer.Return.Errors = true
	c.Producer.Timeout = 10 * time.Second
	client, err := sarama.NewClient(cfg.Brokers, c)
	if err != nil {
		return nil, fmt.Errorf("[NewKafkaClient] Brokers [%v]. Error: %v", strings.Join(cfg.Brokers, ", "), err)
	}
	return &KafkaClient{client}, nil
}
