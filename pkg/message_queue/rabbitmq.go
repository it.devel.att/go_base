package message_queue

import (
	"github.com/streadway/amqp"

	"go_base/pkg/configs"
)

type RabbitMQClient struct {
	Connection *amqp.Connection
}


func NewRabbitMQConnection(cfg configs.RabbitMQ) (*RabbitMQClient, error) {
	conn, err := amqp.Dial(cfg.DSN)
	if err != nil {
		return nil, err
	}
	return &RabbitMQClient{conn}, nil
}
