package tracing

import (
	"encoding/json"
	"fmt"
	"net/http"

	"contrib.go.opencensus.io/exporter/jaeger"
	"go.opencensus.io/trace"

	"go_base/pkg/configs"
)

const TraceHTTPHeader = "X-Trace-Span-Context"

func InitJaeger(cfg configs.Trace) {
	exporter, err := jaeger.NewExporter(jaeger.Options{
		AgentEndpoint: cfg.AgentEndpoint,
		Process: jaeger.Process{
			ServiceName: cfg.ServiceName,
			Tags: []jaeger.Tag{
				jaeger.StringTag("environment", cfg.Environment),
			},
		},
	})
	if err != nil {
		fmt.Println(err)
	}
	trace.RegisterExporter(exporter)
	trace.ApplyConfig(trace.Config{
		// TODO Just for example trace all!
		// TODO Configure it by config
		DefaultSampler: trace.AlwaysSample(),
	})
}

func GetSpanContextFromRequest(r *http.Request) trace.SpanContext {
	spanContextJson := r.Header.Get(TraceHTTPHeader)
	var spanContext trace.SpanContext
	_ = json.Unmarshal([]byte(spanContextJson), &spanContext)
	return spanContext
}

func SetSpanContextToRequest(r *http.Request, span *trace.Span) {
	spanContextJson, _ := json.Marshal(span.SpanContext())
	r.Header.Set(TraceHTTPHeader, string(spanContextJson))
}
