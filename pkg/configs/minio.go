package configs

type Minio struct {
	Endpoint        string `mapstructure:"endpoint" json:"endpoint"`
	AccessKeyID     string `mapstructure:"access_key_id" json:"-"`
	SecretAccessKey string `mapstructure:"secret_access_key" json:"-"`
	UseSSL          bool   `mapstructure:"use_ssl" json:"use_ssl"`
}
