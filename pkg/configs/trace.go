package configs

type Trace struct {
	ServiceName   string `mapstructure:"service_name"`
	AgentEndpoint string `mapstructure:"agent_endpoint"`
	Environment   string `mapstructure:"environment"`
}
