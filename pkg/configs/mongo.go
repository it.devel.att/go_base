package configs

import "time"

type Mongo struct {
	DSN                         string        `mapstructure:"dsn"`
	Database                    string        `mapstructure:"database"`
	DefaultConnectOrDropTimeout time.Duration `mapstructure:"default_connect_or_drop_timeout"`
}
