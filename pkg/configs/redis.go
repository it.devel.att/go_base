package configs

import "time"

type Redis struct {
	DSN string `mapstructure:"dsn"`
	DefaultRequestTimeout time.Duration `mapstructure:"default_request_timeout"`
}
