package configs

type Kafka struct {
	Brokers []string `mapstructure:"brokers" json:"brokers"`
}
