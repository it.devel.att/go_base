package configs

import (
	"fmt"
	"strings"
	"time"

	"github.com/mitchellh/mapstructure"
	"github.com/spf13/viper"
)

type Config struct {
	AppServer    BaseServer `mapstructure:"app_server" json:"app_server"`
	MetricServer BaseServer `mapstructure:"metric_server" json:"metric_server"`
	ClickHouse   Clickhouse `mapstructure:"clickhouse" json:"clickhouse"`
	Postgres     Postgres   `mapstructure:"postgres" json:"clickhouse"`
	Mongo        Mongo      `mapstructure:"mongo" json:"mongo"`
	Minio        Minio      `mapstructure:"minio" json:"minio"`
	ETCD         ETCD       `mapstructure:"etcd" json:"etcd"`
	Redis        Redis      `mapstructure:"redis" json:"redis"`
	Kafka        Kafka      `mapstructure:"kafka" json:"kafka"`
	RabbitMQ     RabbitMQ   `mapstructure:"rabbitmq" json:"rabbitmq"`
	Trace        Trace      `mapstructure:"trace" json:"trace"`
}

func NewConfig() (*Config, error) {
	cfg := &Config{}
	v := viper.New()

	v.SetDefault("kafka.brokers", []string{"kafka:9092"})

	v.SetDefault("clickhouse.dsn", "clickhouse")

	v.SetDefault("postgres.dsn", "postgresql://root:root@127.0.0.1:5433/db")

	v.SetDefault("mongo.dsn", "clickhouse")
	v.SetDefault("mongo.database", "example")
	v.SetDefault("mongo.default_connect_or_drop_timeout", time.Second*5)

	v.SetDefault("redis.dsn", "redis")
	v.SetDefault("redis.default_request_timeout", time.Second)

	v.SetDefault("minio.endpoints", []string{"localhost:9000"})
	v.SetDefault("minio.access_key_id", "minio")
	v.SetDefault("minio.secret_access_key", "minio123")
	v.SetDefault("minio.use_ssl", false)

	v.SetDefault("etcd.endpoints", []string{"localhost:9000"})
	v.SetDefault("etcd.dial_timeout", time.Second)

	v.SetDefault("app_server.port", 9999)
	v.SetDefault("metric_server.port", 2113)

	v.SetDefault("trace.service_name", "service-example-name")
	v.SetDefault("trace.agent_endpoint", "localhost:6831")
	v.SetDefault("trace.environment", "development")

	v.AutomaticEnv()
	v.SetEnvKeyReplacer(strings.NewReplacer(".", "_", "-", "_"))
	if err := v.Unmarshal(cfg, viper.DecodeHook(
		mapstructure.ComposeDecodeHookFunc(
			mapstructure.StringToTimeDurationHookFunc(),
			mapstructure.StringToSliceHookFunc(","),
		))); err != nil {
		return nil, fmt.Errorf("cannot unmarshal config into struct: %w", err)
	}
	return cfg, nil

}
