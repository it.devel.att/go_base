package configs

type BaseServer struct {
	Port int `mapstructure:"port" json:"port"`
}
