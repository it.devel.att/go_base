package configs

import "time"

type ETCD struct {
	Endpoints   []string      `mapstructure:"endpoints" json:"endpoints"`
	DialTimeout time.Duration `mapstructure:"dial_timeout" json:"dial_timeout"`
}
