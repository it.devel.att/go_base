package dependencies

import (
	"go.uber.org/zap"
	"go_base/pkg/circuit_breaker"
	"go_base/pkg/server/middleware"
)

var Init Initializer = initializer{}

//go:generate mockgen -source init.go -destination mock/init.go
type (
	Initializer interface {
		NewDependencies() Dependencies
	}

	initializer struct{}
)

func (initializer) NewDependencies() Dependencies {
	log, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}
	return &dependencies{
		log:     log.Named("dependencies"),
		circuitBreakers: map[string]circuit_breaker.CircuitBreaker{},
		metricMiddlewares: map[string]middleware.Middleware{},
		traceMiddlewares: map[string]middleware.Middleware{},
		onClose: []func(){},
	}
}
