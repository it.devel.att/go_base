package dependencies

import (
	"go_base/pkg/tracing"
	"os"
	"os/signal"
	"syscall"
	"time"

	"go.uber.org/zap"

	"go_base/pkg/circuit_breaker"
	"go_base/pkg/configs"
	"go_base/pkg/db"
	"go_base/pkg/metric"
	"go_base/pkg/server"
	"go_base/pkg/server/middleware"
)

//go:generate mockgen -source init.go -destination mock/dependencies.go
type (
	Dependencies interface {
		Config() *configs.Config
		PostgresClient() *db.PostgresClient

		REDMetric() metric.REDMetric
		RepositoryMetric() metric.RepositoryMetric
		UpMetric() metric.UpMetric

		LogMiddleware() middleware.Middleware
		PanicRecoverMiddleware() middleware.Middleware
		MetricMiddleware(server string) middleware.Middleware
		TraceMiddleware(serviceName string) middleware.Middleware

		CircuitBreaker(service string) circuit_breaker.CircuitBreaker

		PrometheusMetricServer() server.Server

		RegisterClosable(func())
		WaitForInterrupt()
		Close()
	}

	dependencies struct {
		log    *zap.Logger
		config *configs.Config

		postgresClient *db.PostgresClient

		redMetric  metric.REDMetric
		upMetric   metric.UpMetric
		repoMetric metric.RepositoryMetric

		logMiddleware          middleware.Middleware
		panicRecoverMiddleware middleware.Middleware
		metricMiddlewares      map[string]middleware.Middleware
		traceMiddlewares       map[string]middleware.Middleware

		circuitBreakers map[string]circuit_breaker.CircuitBreaker

		prometheusMetricServer server.Server

		onClose []func()
	}
)

func NewDependencies() Dependencies {
	return Init.NewDependencies()
}

func (d *dependencies) Config() *configs.Config {
	var err error
	if d.config == nil {
		message := "initialize config"
		if d.config, err = configs.NewConfig(); err != nil {
			d.log.Panic(message, zap.Error(err))
		}
		tracing.InitJaeger(d.config.Trace)
		d.log.Info(message)
	}
	return d.config
}

func (d *dependencies) PostgresClient() *db.PostgresClient {
	var err error
	if d.postgresClient == nil {
		message := "initialize postgres client"
		if d.postgresClient, err = db.NewPostgresClient(d.Config().Postgres); err != nil {
			d.log.Panic(message, zap.Error(err))
		}
		d.log.Info(message)
	}
	return d.postgresClient
}

func (d *dependencies) REDMetric() metric.REDMetric {
	if d.redMetric == nil {
		d.redMetric = metric.NewREDMetric()
		d.log.Info("initialize red metric")
	}
	return d.redMetric
}

func (d *dependencies) RepositoryMetric() metric.RepositoryMetric {
	if d.repoMetric == nil {
		d.repoMetric = metric.NewRepositoryMetric()
		d.log.Info("initialize repository metric")
	}
	return d.repoMetric
}

func (d *dependencies) UpMetric() metric.UpMetric {
	if d.upMetric == nil {
		d.upMetric = metric.NewUPMetric()
		d.log.Info("initialize up metric")
	}
	return d.upMetric
}

func (d *dependencies) PrometheusMetricServer() server.Server {
	var err error
	if d.prometheusMetricServer == nil {
		message := "initialize prometheus metric server"
		if d.prometheusMetricServer, err = server.NewMetricServer(d.log, d.Config().MetricServer); err != nil {
			d.log.Panic(message, zap.Error(err))
		}
		d.RegisterClosable(func() {
			message := "close prometheus metric server"
			if err := d.prometheusMetricServer.Stop(); err != nil {
				d.log.Error(message, zap.Error(err))
				return
			}
			d.log.Info(message)
		})
		d.log.Info(message)
	}
	return d.prometheusMetricServer
}

func (d *dependencies) CircuitBreaker(service string) circuit_breaker.CircuitBreaker {
	if _, ok := d.circuitBreakers[service]; !ok {
		d.circuitBreakers[service] = circuit_breaker.NewConstantCircuitBreaker(
			service,
			d.UpMetric(),
			// TODO Params set from config!
			time.Minute,
			time.Second*30,
			10,
		)
	}
	return d.circuitBreakers[service]
}

func (d *dependencies) PanicRecoverMiddleware() middleware.Middleware {
	if d.panicRecoverMiddleware == nil {
		d.panicRecoverMiddleware = middleware.NewPanicRecoverMiddleware(d.log)
		d.log.Info("initialize panic recover middleware")
	}
	return d.panicRecoverMiddleware
}

func (d *dependencies) LogMiddleware() middleware.Middleware {
	if d.logMiddleware == nil {
		d.logMiddleware = middleware.NewLOGMiddleware(d.log)
		d.log.Info("initialize log middleware")
	}
	return d.logMiddleware
}

func (d *dependencies) MetricMiddleware(serverName string) middleware.Middleware {
	if _, ok := d.metricMiddlewares[serverName]; !ok {
		d.metricMiddlewares[serverName] = middleware.NewMetricMiddleware(serverName, d.REDMetric())
		d.log.Info("initialize metric middleware", zap.String("server", serverName))
	}
	return d.metricMiddlewares[serverName]
}

func (d *dependencies) TraceMiddleware(serviceName string) middleware.Middleware {
	if _, ok := d.traceMiddlewares[serviceName]; !ok {
		d.traceMiddlewares[serviceName] = middleware.NewTraceMiddleware(serviceName)
		d.log.Info("initialize trace middleware", zap.String("serviceName", serviceName))
	}
	return d.traceMiddlewares[serviceName]
}

func (d *dependencies) WaitForInterrupt() {
	shutdownChan := make(chan os.Signal)
	signal.Notify(shutdownChan, syscall.SIGINT, syscall.SIGTERM)
	d.log.Info("Wait for receive interrupt signal")
	<-shutdownChan
	d.log.Info("Receive interrupt signal")
}

func (d *dependencies) RegisterClosable(close func()) {
	d.onClose = append(d.onClose, close)
}

func (d *dependencies) Close() {
	// Close dependencies in reverse order
	for i := len(d.onClose) - 1; i >= 0; i-- {
		d.onClose[i]()
	}
}
