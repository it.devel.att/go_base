package clock

import (
	"time"
)

//go:generate mockgen -source clock.go -destination mock/clock.go
type (
	// Wrapper under global time package for easy mock and tests
	Clock interface {
		Now() time.Time
		NewTimer(d time.Duration) *time.Timer
		NewTicker(d time.Duration) *time.Ticker
		ParseDuration(d string) (time.Duration, error)
		Sleep(d time.Duration)
	}

	// In future we can add some dependencies as timezone, etc...
	clock struct{}
)

// Return new Clock interface
func NewClock() Clock {
	return newClock()
}

func newClock() clock {
	return clock{}
}

// Now returns the current local time.
func (c clock) Now() time.Time {
	return time.Now()
}

// NewTimer returns reference to golang Timer
func (c clock) NewTimer(d time.Duration) *time.Timer {
	return time.NewTimer(d)
}

// NewTicker returns reference to golang Ticker
func (c clock) NewTicker(d time.Duration) *time.Ticker {
	return time.NewTicker(d)
}

func (c clock) Sleep(d time.Duration) {
	time.Sleep(d)
}

func (c clock) ParseDuration(d string) (time.Duration, error) {
	return time.ParseDuration(d)
}

// InitializeGlobalApplicationTimeZone change global time.Local by given config
//
// Use this function on application start (initialization). Panic on wrong TimeZone from config
//func InitializeGlobalApplicationTimeZone(cfg config.App) {
//	loc, err := time.LoadLocation(cfg.TimeZone)
//	if err != nil {
//		panic(err)
//	}
//	time.Local = loc
//}

// Timezone return application timezone in string format. If time.Local is nil return 'UTC' as timezone
//
// Be careful and use this function only after InitializeGlobalApplicationTimeZone was called
// for get relevant application timezone
func Timezone() string {
	if time.Local == nil {
		return "UTC"
	}
	return time.Local.String()
}
