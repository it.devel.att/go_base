package server

import (
	"context"
	"fmt"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net"
	"net/http"
	"sync"
	"time"

	"go.uber.org/zap"

	"go_base/pkg/configs"
)

type metricServer struct {
	log      *zap.Logger
	cfg      configs.BaseServer
	wg       sync.WaitGroup
	listener net.Listener
	server   *http.Server
}

func NewMetricServer(log *zap.Logger, cfg configs.BaseServer) (Server, error) {
	var err error

	handler := http.NewServeMux()
	handler.Handle("/metrics", promhttp.Handler())

	listener, err := net.Listen("tcp", fmt.Sprintf(":%v", cfg.Port))
	if err != nil {
		return nil, fmt.Errorf("cannot listen metrics port: %w", err)
	}
	server := &metricServer{
		log: log.Named("metric_server"),
		cfg: cfg,
		server: &http.Server{
			Handler: handler,
		},
		listener: listener,
	}
	return server, nil
}

func (s *metricServer) Start() {
	s.log.Info("Start metric server", zap.Int("port", s.cfg.Port))
	s.wg.Add(1)
	go func() {
		defer s.wg.Done()

		if err := s.server.Serve(s.listener); err != nil && err != http.ErrServerClosed {
			s.log.Panic("Error while serve metric server", zap.Error(err))
		}
	}()
}

func (s *metricServer) Stop() error {
	s.log.Info("Stop metric server")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	if err := s.server.Shutdown(ctx); err != nil {
		return err
	}
	s.wg.Wait()
	return nil
}
