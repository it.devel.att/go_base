package server

import (
	"context"
	"fmt"
	"net"
	"net/http"

	"go.uber.org/zap"

	"go_base/pkg/configs"
	"go_base/pkg/server/middleware"
)

type BaseServer struct {
	Cfg        configs.BaseServer
	Log        *zap.Logger
	Name       string
	Listener   net.Listener
	HTTPServer *http.Server
}

func NewServer(
	name string,
	cfg configs.BaseServer,
	log *zap.Logger,
	handler http.Handler,
	middlewares ...middleware.Middleware,
) (HTTPServer, error) {
	return newServer(name, cfg, log, handler, middlewares...)
}

func newServer(
	name string,
	cfg configs.BaseServer,
	log *zap.Logger,
	handler http.Handler,
	middlewares ...middleware.Middleware,
) (*BaseServer, error) {
	serv := &BaseServer{Cfg: cfg}
	ln, err := net.Listen("tcp", fmt.Sprintf(":%v", serv.Cfg.Port))
	if err != nil {
		return nil, fmt.Errorf("cannot listen port: %w", err)
	}
	serv.Log = log.Named(name)
	serv.Name = name
	serv.Listener = ln
	for i := len(middlewares) - 1; i >= 0; i-- {
		handler = middlewares[i].WrapHandler(handler)
	}
	serv.HTTPServer = &http.Server{Handler: handler}
	return serv, nil
}

func (s *BaseServer) Start() {
	go func() {
		s.Log.Info("starting server", zap.String("name", s.Name), zap.String("address", s.Listener.Addr().String()))
		err := s.HTTPServer.Serve(s.Listener)
		if err != nil && err != http.ErrServerClosed {
			s.Log.Error("http server error", zap.String("name", s.Name), zap.Error(err))
		}
	}()
}

func (s *BaseServer) Shutdown(ctx context.Context) error {
	return s.HTTPServer.Shutdown(ctx)
}
