package middleware

import (
	"net/http"
	"runtime/debug"

	"go.uber.org/zap"
)

type (
	logMiddleware struct {
		log *zap.Logger
	}
)

type statusRecorder struct {
	http.ResponseWriter
	Status int
}

func (r *statusRecorder) WriteHeader(status int) {
	r.Status = status
	r.ResponseWriter.WriteHeader(status)
}

func NewLOGMiddleware(log *zap.Logger) Middleware {
	return newLOGMiddleware(log)
}

func newLOGMiddleware(log *zap.Logger) *logMiddleware {
	return &logMiddleware{
		log: log.Named("log_middleware"),
	}
}

func (m *logMiddleware) WrapHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		recorder := &statusRecorder{ResponseWriter: w, Status: 200}
		defer func() {
			var zapFields []zap.Field

			zapFields = append(
				zapFields,
				zap.String("method", r.Method),
				zap.String("path", r.URL.RequestURI()),
				zap.Int("status", recorder.Status),
				zap.String("remote_addr", r.RemoteAddr),
				zap.String("user_agent", r.UserAgent()),
			)
			if rec := recover(); rec != nil {
				recorder.WriteHeader(500)
				zapFields = append(
					zapFields,
					zap.String("stack_trace", string(debug.Stack())),
				)

				m.log.Info("Handled request. Unexpected Panic", zapFields...)
			} else {
				m.log.Info("Handled request", zapFields...)
			}

		}()
		next.ServeHTTP(recorder, r)
	})
}
