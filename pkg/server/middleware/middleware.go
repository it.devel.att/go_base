package middleware

import "net/http"

type Middleware interface {
	WrapHandler(next http.Handler) http.Handler
}
