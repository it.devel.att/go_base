package middleware

import (
	"fmt"
	"net/http"
	"runtime/debug"

	"go.uber.org/zap"
)

type (
	panicMiddleware struct {
		log *zap.Logger
	}
)

func NewPanicRecoverMiddleware(log *zap.Logger) Middleware {
	return newPanicRecoverMiddleware(log)
}

func newPanicRecoverMiddleware(log *zap.Logger) *panicMiddleware {
	return &panicMiddleware{
		log: log.Named("panic_recover_middleware"),
	}
}

func (m *panicMiddleware) WrapHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if rec := recover(); rec != nil {
				w.WriteHeader(500)
				m.log.Debug(
					"Unexpected Panic",
					zap.String("recover", fmt.Sprint(rec)),
					zap.String("stack_trace", string(debug.Stack())),
				)
			}
		}()
		next.ServeHTTP(w, r)
	})
}
