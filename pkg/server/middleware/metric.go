package middleware

import (
	"net/http"
	"strconv"

	"go_base/pkg/clock"
	"go_base/pkg/metric"
)

type (
	metricMiddleware struct {
		serverName string
		time       clock.Clock
		metric     metric.REDMetric
	}
)

type metricRecorder struct {
	http.ResponseWriter
	Status int
}

func (r *metricRecorder) WriteHeader(status int) {
	r.Status = status
	r.ResponseWriter.WriteHeader(status)
}

func NewMetricMiddleware(serverName string, metric metric.REDMetric) Middleware {
	return newMetricMiddleware(serverName, metric, clock.NewClock())
}

func newMetricMiddleware(serverName string, metric metric.REDMetric, time clock.Clock) *metricMiddleware {
	return &metricMiddleware{
		serverName: serverName,
		metric:     metric,
		time:       time,
	}
}

func (m *metricMiddleware) WrapHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		recorder := &metricRecorder{ResponseWriter: w, Status: 200}
		start := m.time.Now()
		defer func() {
			statusCode := strconv.Itoa(recorder.Status)
			m.metric.RequestInc(m.serverName, statusCode)
			m.metric.Duration(start, m.serverName, statusCode)
			// TODO Looks a little strange ;)
			if recorder.Status >= 500 {
				m.metric.ErrorInc(m.serverName, statusCode)
			}
		}()
		next.ServeHTTP(recorder, r)
	})
}
