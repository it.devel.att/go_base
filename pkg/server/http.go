package server

import "context"

type HTTPServer interface {
	Start()
	Shutdown(ctx context.Context) error
}
