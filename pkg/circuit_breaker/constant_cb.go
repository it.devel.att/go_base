package circuit_breaker

import (
	"errors"
	"sync"
	"sync/atomic"
	"time"

	"go_base/pkg/clock"
	"go_base/pkg/metric"
	"go_base/pkg/random"
)

type (
	circuitBreakerState int

	CircuitBreaker interface {
		IsOpen() bool
		IsHalfOpen() bool
		PassRequest() bool
		ErrorInc()
	}

	constantCircuitBreaker struct {
		serviceName       string
		errCountForOpenCB uint64

		metric     metric.UpMetric
		time       clock.Clock
		rand       random.Random
		errCounter uint64

		openDuration     time.Duration
		halfOpenDuration time.Duration

		mu                  sync.RWMutex
		state               circuitBreakerState
		lastTimeErrOccurred time.Time
	}
)

const (
	closeState circuitBreakerState = iota
	openState
	halfOpenState
)

var ErrCircuitBreakerOpen = errors.New("open circuit breaker")

func NewConstantCircuitBreaker(
	serviceName string,
	m metric.UpMetric,
	openDuration time.Duration,
	halfOpenDuration time.Duration,
	errCountForOpenCB uint64,
) CircuitBreaker {
	return newConstantCircuitBreaker(
		serviceName,
		m,
		clock.NewClock(),
		random.NewRandom(),
		openDuration,
		halfOpenDuration,
		errCountForOpenCB,
	)
}

func newConstantCircuitBreaker(
	serviceName string,
	m metric.UpMetric,
	t clock.Clock,
	r random.Random,
	openDuration time.Duration,
	halfOpenDuration time.Duration,
	errCountForOpenCB uint64,
) *constantCircuitBreaker {
	return &constantCircuitBreaker{
		serviceName:       serviceName + "_circuit_breaker",
		metric:            m,
		time:              t,
		rand:              r,
		openDuration:      openDuration,
		halfOpenDuration:  halfOpenDuration,
		errCountForOpenCB: errCountForOpenCB,
	}
}

func (cb *constantCircuitBreaker) IsOpen() bool {
	cb.mu.Lock()
	defer cb.mu.Unlock()

	if cb.state == openState && cb.lastTimeErrOccurred.Before(cb.time.Now().Add(-cb.openDuration)) {
		cb.state = halfOpenState
		cb.lastTimeErrOccurred = cb.time.Now()
		cb.resetErrCounter()
	}
	if cb.state == closeState {
		cb.metric.Down(cb.serviceName)
	}
	return cb.state == openState
}

func (cb *constantCircuitBreaker) IsHalfOpen() bool {
	cb.mu.Lock()
	defer cb.mu.Unlock()

	if cb.state == halfOpenState &&
		cb.lastTimeErrOccurred.Before(cb.time.Now().Add(-cb.halfOpenDuration)) &&
		atomic.CompareAndSwapUint64(&cb.errCounter, 0, 0) {
		cb.state = closeState
		cb.metric.Down(cb.serviceName)
		cb.resetErrCounter()
	}
	return cb.state == halfOpenState
}

func (cb *constantCircuitBreaker) isClose() bool {
	cb.mu.RLock()
	defer cb.mu.RUnlock()
	return cb.state == closeState
}

func (cb *constantCircuitBreaker) ErrorInc() {
	if (atomic.AddUint64(&cb.errCounter, 1) > cb.errCountForOpenCB && cb.isClose()) || cb.IsHalfOpen() {
		cb.setOpen()
	}
}

func (cb *constantCircuitBreaker) setOpen() {
	cb.mu.Lock()
	cb.lastTimeErrOccurred = cb.time.Now()
	cb.state = openState
	cb.metric.Up(cb.serviceName)
	cb.resetErrCounter()
	cb.mu.Unlock()
}

func (cb *constantCircuitBreaker) resetErrCounter() {
	atomic.CompareAndSwapUint64(&cb.errCounter, atomic.AddUint64(&cb.errCounter, 0), 0)
}

// TODO Make configurable
func (cb *constantCircuitBreaker) PassRequest() bool {
	return cb.rand.Intn(2) == 0
}
