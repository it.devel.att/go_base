package metric

import (
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

type (
	RepositoryMetric interface {
		UseInc(repository, operation string)
		ErrorInc(repository, operation string)
		Duration(start time.Time, repository, operation string)
	}

	repositoryMetric struct {
		use      *prometheus.CounterVec
		error    *prometheus.CounterVec
		duration *prometheus.SummaryVec
	}
)

var repoLabels = []string{"repository", "operation"}

func NewRepositoryMetric() RepositoryMetric {
	return newRepositoryMetric()
}

func newRepositoryMetric() *repositoryMetric {
	return &repositoryMetric{
		use: promauto.NewCounterVec(
			prometheus.CounterOpts{
				Name: "repository_use_total",
				Help: "Amount of total use of repository",
			},
			repoLabels,
		),
		error: promauto.NewCounterVec(
			prometheus.CounterOpts{
				Name: "repository_error",
				Help: "Amount of failed calls to repository",
			},
			repoLabels,
		),
		duration: promauto.NewSummaryVec(
			prometheus.SummaryOpts{
				Name:       "repository_duration",
				Help:       "Repository methods duration",
				Objectives: map[float64]float64{0.5: 0.05, 0.95: 0.01, 0.99: 0.001},
				MaxAge:     time.Minute,
			},
			repoLabels,
		),
	}
}

func (r *repositoryMetric) UseInc(repository, operation string) {
	r.use.WithLabelValues(repository, operation).Inc()
}

func (r *repositoryMetric) ErrorInc(repository, operation string) {
	r.error.WithLabelValues(repository, operation).Inc()
}

func (r *repositoryMetric) Duration(start time.Time, repository, operation string) {
	r.duration.WithLabelValues(repository, operation).Observe(float64(time.Since(start).Milliseconds()))
}
