package metric

import (
	"github.com/prometheus/client_golang/prometheus/promauto"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

type (
	REDMetric interface {
		RequestInc(server, code string)
		ErrorInc(server, code string)
		Duration(start time.Time, server, code string)
	}

	redMetric struct {
		request  *prometheus.CounterVec
		error    *prometheus.CounterVec
		duration *prometheus.SummaryVec
	}
)

var labels = []string{"server", "code"}

func NewREDMetric() REDMetric {
	return newREDMetric()
}

func newREDMetric() *redMetric {
	return &redMetric{
		request: promauto.NewCounterVec(
			prometheus.CounterOpts{
				Name: "http_request_total",
				Help: "Amount of http request total",
			},
			labels,
		),
		error: promauto.NewCounterVec(
			prometheus.CounterOpts{
				Name: "http_request_error",
				Help: "Amount of failed http requests",
			},
			labels,
		),
		duration: promauto.NewSummaryVec(
			prometheus.SummaryOpts{
				Name:       "http_request_duration",
				Help:       "Request duration",
				Objectives: map[float64]float64{0.5: 0.05, 0.95: 0.01, 0.99: 0.001},
				MaxAge:     time.Minute,
			},
			labels,
		),
	}
}

func (r *redMetric) RequestInc(server, code string) {
	r.request.WithLabelValues(server, code).Inc()
}

func (r *redMetric) ErrorInc(server, code string) {
	r.error.WithLabelValues(server, code).Inc()
}

func (r *redMetric) Duration(start time.Time, server, code string) {
	r.duration.WithLabelValues(server, code).Observe(float64(time.Since(start).Milliseconds()))
}
