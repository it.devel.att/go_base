package metric

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

type (
	UpMetric interface {
		Up(service string)
		Down(service string)
	}

	upMetric struct {
		up *prometheus.GaugeVec
	}
)

var upDownLabels = []string{"service"}

func NewUPMetric() UpMetric {
	return newUpMetric()
}

func newUpMetric() *upMetric {
	return &upMetric{
		up: promauto.NewGaugeVec(
			prometheus.GaugeOpts{
				Name: "service_up",
				Help: "Service active",
			},
			upDownLabels,
		),
	}
}

func (r *upMetric) Up(service string) {
	r.up.WithLabelValues(service).Set(1)
}

func (r *upMetric) Down(service string) {
	r.up.WithLabelValues(service).Set(0)
}
