package random

import "math/rand"

//go:generate mockgen -source rand.go -destination mock/rand.go
type (
	// Wrapper under global math/rand for easy mock and tests
	Random interface {
		Intn(n int) int
		// TODO Implements other methods when required
	}

	random struct{}
)

func NewRandom() Random {
	return newRand()
}

func newRand() random {
	return random{}
}

func (random) Intn(n int) int {
	return rand.Intn(n)
}
