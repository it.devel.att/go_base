package db

import (
	"context"
	"github.com/go-redis/redis/v8"

	"go_base/pkg/configs"
)

func NewRedisClient(cfg configs.Redis) (*redis.Client, error) {
	opts, err := redis.ParseURL(cfg.DSN)
	if err != nil {
		return nil, err
	}
	ctx, cancel := context.WithTimeout(context.Background(), cfg.DefaultRequestTimeout)
	defer cancel()
	client := redis.NewClient(opts)

	if err := client.Ping(ctx).Err(); err != nil {
		return nil, err
	}
	return client, nil
}
