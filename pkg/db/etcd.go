package db

import (
	"go.etcd.io/etcd/client/v3"

	"go_base/pkg/configs"
)

func NewETCDClient(cfg configs.ETCD) (*clientv3.Client, error) {
	client, err := clientv3.New(clientv3.Config{
		Endpoints:   cfg.Endpoints,
		DialTimeout: cfg.DialTimeout,
	})
	if err != nil {
		return nil, err
	}
	return client, nil

}
