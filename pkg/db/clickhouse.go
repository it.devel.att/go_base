package db

import (
	"fmt"

	_ "github.com/ClickHouse/clickhouse-go"
	"github.com/jmoiron/sqlx"

	"go_base/pkg/configs"
)

func NewClickHouseClient(cfg configs.Clickhouse) (*sqlx.DB, error) {
	client, err := sqlx.Connect("clickhouse", cfg.DSN)
	if err != nil {
		return nil, fmt.Errorf("cannot connect to the clickhouse database at %s : %w", cfg.DSN, err)
	}
	if err = client.Ping(); err != nil {
		return nil, fmt.Errorf("clickhouse database is unavailable at %s: %w", cfg.DSN, err)
	}
	// TODO Need to set this values from config
	//cxDB.SetMaxOpenConns()
	//cxDB.SetMaxIdleConns()
	//cxDB.SetConnMaxLifetime()
	return client, nil
}
