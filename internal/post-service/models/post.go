package models

type (
	Post struct {
		ID          int    `db:"id" json:"id"`
		Title       string `db:"title" json:"title"`
		Description string `db:"description" json:"description"`
	}
)
