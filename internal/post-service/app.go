package post_service

import (
	. "go_base/internal/post-service/dependencies"
	"go_base/pkg/dependencies"
)

func StartApp() {
	deps := NewDependencies(dependencies.NewDependencies())

	metric := deps.PrometheusMetricServer()
	app := deps.PostServer()

	metric.Start()
	app.Start()

	deps.WaitForInterrupt()
	deps.Close()
}
