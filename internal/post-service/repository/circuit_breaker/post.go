package circuit_breaker

import (
	"context"
	"go_base/internal/post-service/models"

	"go.opencensus.io/trace"

	"go_base/internal/post-service/repository"
	"go_base/pkg/circuit_breaker"
)

type (
	PostRepoCircuitBreaker struct {
		repository.PostRepository
		circuit_breaker.CircuitBreaker
	}
)

var _ repository.PostRepository = &PostRepoCircuitBreaker{}

func WrapPostRepository(
	repo repository.PostRepository,
	cb circuit_breaker.CircuitBreaker,
) *PostRepoCircuitBreaker {
	return &PostRepoCircuitBreaker{
		PostRepository: repo,
		CircuitBreaker: cb,
	}
}

func (cb *PostRepoCircuitBreaker) List(ctx context.Context) ([]*models.Post, error) {
	spanCtx, span := trace.StartSpan(ctx, "*postRepoCircuitBreaker.List")
	defer span.End()

	// TODO This condition (cb.IsOpen() || (cb.IsHalfOpen() && !cb.PassRequest())) put in some base method!!
	if cb.IsOpen() || (cb.IsHalfOpen() && !cb.PassRequest()) {
		span.AddAttributes(trace.StringAttribute("error", circuit_breaker.ErrCircuitBreakerOpen.Error()))
		return nil, circuit_breaker.ErrCircuitBreakerOpen
	}
	posts, err := cb.PostRepository.List(spanCtx)
	if err != nil {
		cb.ErrorInc()
		span.AddAttributes(trace.StringAttribute("error", err.Error()))
		return posts, err
	}
	return posts, nil
}

func (cb *PostRepoCircuitBreaker) Create(ctx context.Context, post *models.Post) error {
	spanCtx, span := trace.StartSpan(ctx, "*postRepoCircuitBreaker.Create")
	defer span.End()

	if cb.IsOpen() || (cb.IsHalfOpen() && !cb.PassRequest()) {
		return circuit_breaker.ErrCircuitBreakerOpen
	}
	if err := cb.PostRepository.Create(spanCtx, post); err != nil {
		cb.ErrorInc()
		span.AddAttributes(trace.StringAttribute("error", err.Error()))
		return err
	}
	return nil
}
