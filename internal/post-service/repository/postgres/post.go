package postgres

import (
	"context"
	"fmt"
	"math/rand"
	"time"

	"github.com/jmoiron/sqlx"
	"go.opencensus.io/trace"
	"go.uber.org/zap"

	"go_base/internal/post-service/models"
	"go_base/internal/post-service/repository"
	"go_base/pkg/clock"
	"go_base/pkg/db"
	"go_base/pkg/metric"
)

type (
	PostRepository struct {
		client *sqlx.DB
		log    *zap.Logger
		time   clock.Clock
		metric metric.RepositoryMetric

		name  string
		table string
	}
)

var _ repository.PostRepository = &PostRepository{}

func NewPostRepository(client *db.PostgresClient, m metric.RepositoryMetric, log *zap.Logger) *PostRepository {
	serviceName := "post_repository"
	return &PostRepository{
		log:    log.Named(serviceName),
		client: client.DB,
		metric: m,
		time:   clock.NewClock(),
		name:   serviceName,
		table:  "posts",
	}
}

func (repo *PostRepository) List(parentCtx context.Context) ([]*models.Post, error) {
	var err error
	ctx, span := trace.StartSpan(parentCtx, "*postRepository.List")
	start, operation := repo.time.Now(), "list"
	defer func() {
		repo.updateMetrics(start, operation, span, err)
	}()

	// TODO Just for check circuit breaker
	if repo.time.Now().Minute()%10 == 0 {
		if rand.Intn(2) == 0 {
			span.AddAttributes(trace.StringAttribute("random_sleep", "true"))
			repo.client.ExecContext(ctx, "select pg_sleep(10)")
		}
	}

	var users []*models.Post

	query := fmt.Sprintf(`SELECT * FROM %v`, repo.table)
	span.AddAttributes(trace.StringAttribute("query", query))

	if err = repo.client.SelectContext(ctx, &users, query); err != nil {
		span.AddAttributes(trace.StringAttribute("error", err.Error()))
		return nil, err
	}
	return users, nil
}

func (repo *PostRepository) Create(parentCtx context.Context, post *models.Post) (err error) {
	ctx, span := trace.StartSpan(parentCtx, "*postRepository.Create")
	start, operation := repo.time.Now(), "create"
	defer func() {
		repo.updateMetrics(start, operation, span, err)
	}()

	query := fmt.Sprintf("INSERT INTO %v (title, description) VALUES ($1, $2) RETURNING *", repo.table)
	span.AddAttributes(trace.StringAttribute("query", query))

	row := repo.client.QueryRowxContext(ctx, query, post.Title, post.Description)
	if row.Err() != nil {
		err = row.Err()
		return
	}
	err = row.StructScan(post)
	return
}

func (repo *PostRepository) updateMetrics(start time.Time, operation string, span *trace.Span, err error) {
	repo.metric.UseInc(repo.name, operation)
	repo.metric.Duration(start, repo.name, operation)
	if err != nil {
		span.AddAttributes(trace.StringAttribute("error", err.Error()))
		repo.log.Error("error", zap.String("method", operation), zap.Error(err))
		repo.metric.ErrorInc(repo.name, operation)
	}
	span.End()
}
