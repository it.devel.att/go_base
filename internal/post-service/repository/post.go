package repository

import (
	"context"

	"go_base/internal/post-service/models"
)

//go:generate mockgen -source init.go -destination mock/post.go
type PostRepository interface {
	List(ctx context.Context) ([]*models.Post, error)
	Create(ctx context.Context, post *models.Post) error
}
