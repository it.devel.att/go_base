// Code generated by MockGen. DO NOT EDIT.
// Source: init.go

// Package mock_dependencies is a generated GoMock package.
package mock_dependencies

import (
	dependencies "go_base/internal/post-service/dependencies"
	dependencies0 "go_base/pkg/dependencies"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

// MockInitializer is a mock of Initializer interface.
type MockInitializer struct {
	ctrl     *gomock.Controller
	recorder *MockInitializerMockRecorder
}

// MockInitializerMockRecorder is the mock recorder for MockInitializer.
type MockInitializerMockRecorder struct {
	mock *MockInitializer
}

// NewMockInitializer creates a new mock instance.
func NewMockInitializer(ctrl *gomock.Controller) *MockInitializer {
	mock := &MockInitializer{ctrl: ctrl}
	mock.recorder = &MockInitializerMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockInitializer) EXPECT() *MockInitializerMockRecorder {
	return m.recorder
}

// NewDependencies mocks base method.
func (m *MockInitializer) NewDependencies(d dependencies0.Dependencies) dependencies.Dependencies {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "NewDependencies", d)
	ret0, _ := ret[0].(dependencies.Dependencies)
	return ret0
}

// NewDependencies indicates an expected call of NewDependencies.
func (mr *MockInitializerMockRecorder) NewDependencies(d interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "NewDependencies", reflect.TypeOf((*MockInitializer)(nil).NewDependencies), d)
}
