package dependencies

import (
	"go.uber.org/zap"
	"go_base/pkg/dependencies"
)

var Init Initializer = initializer{}

//go:generate mockgen -source init.go -destination mock/init.go
type (
	Initializer interface {
		NewDependencies(d dependencies.Dependencies) Dependencies
	}

	initializer struct{}
)

func (initializer) NewDependencies(d dependencies.Dependencies) Dependencies {
	log, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}
	return &deps{
		Dependencies: d,
		log: log.Named("example.dependencies"),
	}
}
