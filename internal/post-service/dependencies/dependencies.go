package dependencies

import (
	"context"
	"net/http"
	"time"

	"go.uber.org/zap"

	"go_base/internal/post-service/handler"
	"go_base/internal/post-service/repository"
	"go_base/internal/post-service/repository/circuit_breaker"
	"go_base/internal/post-service/repository/postgres"
	"go_base/internal/post-service/service"
	"go_base/pkg/dependencies"
	"go_base/pkg/server"
)

type (
	Dependencies interface {
		dependencies.Dependencies

		PostRepository() repository.PostRepository
		PostService() service.PostService
		PostHandler() http.Handler
		PostServer() server.HTTPServer
		Close()
	}

	deps struct {
		log *zap.Logger
		dependencies.Dependencies

		postRepository         repository.PostRepository
		postRepoCircuitBreaker repository.PostRepository

		postService service.PostService
		postHandler http.Handler
		postServer  server.HTTPServer
	}
)

func NewDependencies(d dependencies.Dependencies) Dependencies {
	return Init.NewDependencies(d)
}

func (d *deps) PostRepository() repository.PostRepository {
	if d.postRepository == nil {
		d.postRepository = postgres.NewPostRepository(d.PostgresClient(), d.RepositoryMetric(), d.log)
		d.log.Info("initialize postgres post repository")
	}
	return d.postRepository
}

func (d *deps) CircuitBreakerPostRepository() repository.PostRepository {
	if d.postRepoCircuitBreaker == nil {
		d.postRepoCircuitBreaker = circuit_breaker.WrapPostRepository(
			d.PostRepository(),
			d.CircuitBreaker("postgres"),
		)
		d.log.Info("initialize post repository circuit breaker")
	}
	return d.postRepoCircuitBreaker
}

func (d *deps) PostService() service.PostService {
	if d.postService == nil {
		d.postService = service.NewPostService(d.CircuitBreakerPostRepository())
		d.log.Info("initialize post service")
	}
	return d.postService
}

func (d *deps) PostHandler() http.Handler {
	if d.postHandler == nil {
		d.postHandler = handler.NewPostHandler(d.PostService())
		d.log.Info("initialize Post handler")
	}
	return d.postHandler
}

func (d *deps) PostServer() server.HTTPServer {
	var err error
	if d.postServer == nil {
		serverName := "post_server"
		if d.postServer, err = server.NewServer(
			serverName,
			d.Config().AppServer,
			d.log,
			d.PostHandler(),
			d.TraceMiddleware("post-service"), d.MetricMiddleware(serverName), d.LogMiddleware(), d.PanicRecoverMiddleware(),
		); err != nil {
			d.log.Panic("initialize post server", zap.Error(err))
		}
		d.RegisterClosable(func() {
			ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
			defer cancel()
			if err := d.postServer.Shutdown(ctx); err != nil {
				d.log.Error("close Post server", zap.Error(err))
				return
			}
			d.log.Info("close Post server")
		})
		d.log.Info("initialize Post server")
	}
	return d.postServer
}
