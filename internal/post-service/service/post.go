package service

import (
	"context"

	"go.opencensus.io/trace"
	"go_base/internal/post-service/models"

	"go_base/internal/post-service/repository"
)

type (
	PostService interface {
		PostList(ctx context.Context) ([]*models.Post, error)
		CreatePost(ctx context.Context, post *models.Post) error
	}

	postService struct {
		postRepo repository.PostRepository
	}
)

func NewPostService(postRepo repository.PostRepository) PostService {
	return newPostService(postRepo)
}

func newPostService(postRepo repository.PostRepository) *postService {
	return &postService{
		postRepo: postRepo,
	}
}

func (s *postService) PostList(ctx context.Context) ([]*models.Post, error) {
	spanCtx, span := trace.StartSpan(ctx, "*postService.PostList")
	defer span.End()

	return s.postRepo.List(spanCtx)
}

func (s *postService) CreatePost(ctx context.Context, post *models.Post) error {
	spanCtx, span := trace.StartSpan(ctx, "*postService.CreatePost")
	defer span.End()

	return s.postRepo.Create(spanCtx, post)
}
