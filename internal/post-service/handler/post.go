package handler

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
	"go.opencensus.io/trace"

	"go_base/internal/post-service/models"
	"go_base/internal/post-service/service"
)

type postHandler struct {
	postService service.PostService
}

func NewPostHandler(postService service.PostService) http.Handler {
	r := mux.NewRouter()
	h := &postHandler{postService}
	r.HandleFunc("/api/v1/posts", h.ListPosts).Methods(http.MethodGet)
	r.HandleFunc("/api/v1/posts", h.CreatePost).Methods(http.MethodPost)
	return r
}

func (h *postHandler) ListPosts(rw http.ResponseWriter, r *http.Request) {
	ctx, span := trace.StartSpan(r.Context(), "*postHandler.ListPosts")
	defer span.End()

	posts, err := h.postService.PostList(ctx)
	if err != nil {
		span.AddAttributes(trace.StringAttribute("err", err.Error()))
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}
	json.NewEncoder(rw).Encode(posts)
}

func (h *postHandler) CreatePost(rw http.ResponseWriter, r *http.Request) {
	ctx, span := trace.StartSpan(r.Context(), "*postHandler.CreatePost")
	defer span.End()

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		span.AddAttributes(trace.StringAttribute("err", err.Error()))
		rw.WriteHeader(400)
		return
	}
	post := &models.Post{}
	err = json.Unmarshal(body, post)
	if err != nil {
		span.AddAttributes(trace.StringAttribute("err", err.Error()))
		rw.WriteHeader(400)
		return
	}
	err = h.postService.CreatePost(ctx, post)
	if err != nil {
		span.AddAttributes(trace.StringAttribute("err", err.Error()))
		rw.WriteHeader(500)
		return
	}
	json.NewEncoder(rw).Encode(post)
}
